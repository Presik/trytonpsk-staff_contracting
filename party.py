#This file is part of Hotel module for Tryton.  The COPYRIGHT file at
#the top level of this repository contains the full copyright notices
#and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    motherHead = fields.Boolean('Mother Head of Home')
    disability = fields.Boolean('Disability')
    # subdivision_born = fields.Char('Subdivision of Born')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
