# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, Workflow, ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, Button, StateTransition

YES_NO_SEL = [
            ('yes', 'Yes'),
            ('no', 'No'),
            ('', ''),
]


class Employee(metaclass=PoolMeta):
    __name__ = 'company.employee'
    salary_level = fields.Many2One('staff.salary_level', 'Salary Level')
    payment_detail = fields.Char('Payment Detail', select=True)
    box_family_date = fields.Date('Box Family Date')
    health_input_date = fields.Date('Health Input Date')
    health_output_date = fields.Date('Health Output Date')
    risk_input_date = fields.Date('Risk Input Date')
    risk_output_date = fields.Date('Risk Output Date')
    hse_training = fields.Date('HSE Training')
    coverage_start = fields.Date('Coverage Start')
    coverage_end = fields.Date('Coverage End')
    uniforms = fields.One2Many('staff.employee.uniform', 'employee', 'Employee Uniform')
    medical_exams = fields.One2Many('staff.employee.medical_exam',
        'employee','Medical Exams')
    syndicate = fields.Selection(YES_NO_SEL, 'Syndicate')
    syndicate_string = syndicate.translated('syndicate')
    work_height = fields.Selection(YES_NO_SEL, 'Work Height')
    work_height_string = work_height.translated('work_height')

    date_uniform_shirt = fields.Function(fields.Date('Shirt Date'), 'get_uniform')
    date_uniform_pants = fields.Function(fields.Date('Pants Date'), 'get_uniform')
    date_uniform_boots = fields.Function(fields.Date('Boots Date'), 'get_uniform')
    size_uniform_shirt = fields.Function(fields.Char('Shirt Size'), 'get_uniform')
    size_uniform_pants = fields.Function(fields.Char('Pants Size'), 'get_uniform')
    size_uniform_boots = fields.Function(fields.Char('Boots Size'), 'get_uniform')
    quantity_uniform_shirt = fields.Function(fields.Integer('Shirt Qty'), 'get_uniform')
    quantity_uniform_pants = fields.Function(fields.Integer('Pants Qty'), 'get_uniform')
    quantity_uniform_boots = fields.Function(fields.Integer('Boots Qty'), 'get_uniform')
    office_specific = fields.Many2One('staff.office_specific', 'Office Specific')

    @classmethod
    def __setup__(cls):
        super(Employee, cls).__setup__()

    def get_uniform(self, name=None):
        pool = Pool()
        config = pool.get('staff.configuration')(1)
        uniforms = dict([(uniform.date, uniform) for uniform in self.uniforms if uniform.date])
        if not uniforms:
            return
        config_element = getattr(config, name[-5:])
        uniform = uniforms[max(uniforms)]
        if not config_element:
            return
        for line in uniform.lines:
            if config_element.id == line.element.id:
                if name[:-14] == 'date':
                    return uniform.date
                return getattr(line, name[:-14])

    def set_default_values(self):
        if not self.category:
            return

        pool = Pool()
        Uniform = pool.get('staff.uniform')

        values = []
        for uniform in self.category.uniform_default:
            val = {
                'employee': self.id,
                'element': uniform.element,
                'quantity': uniform.quantity,
                'description': uniform.description,
                'size': uniform.size,
            }
            values.append(val)
        if values:
            Uniform.create(values)


class EmployeeCategory(metaclass=PoolMeta):
    __name__ = 'staff.employee_category'
    uniforms = fields.Many2Many(
            'employee_category-staff.uniform',
            'employee_category', 'uniform',
            'Employee Category - Uniform')
    medical_exams = fields.Many2Many(
            'employee_category-staff.medical_exam',
            'employee_category', 'medical_exam',
            'Employee Category - Medical Exam')


class EmployeeCategoryUniformDefault(ModelSQL):
    'Employee Category - Uniform Default'
    __name__ = 'employee_category-staff.uniform'
    _table = 'employee_category_staff_uniform'
    employee_category = fields.Many2One('staff.employee_category',
            'Employee Category', select=True, required=True, ondelete='CASCADE')
    uniform = fields.Many2One('staff.uniform.element', 'Uniform',
            ondelete='CASCADE', select=True, required=True)


class EmployeeCategoryMedicalExamDefault(ModelSQL):
    'Employee Category - Medical Exam Default'
    __name__ = 'employee_category-staff.medical_exam'
    _table = 'employee_category_staff_medical_exam'
    employee_category = fields.Many2One('staff.employee_category',
            'Employee Category', select=True, required=True,
            ondelete='CASCADE')
    medical_exam = fields.Many2One('staff.medical_exam',
            'Medical Exam', ondelete='CASCADE', select=True,
            required=True)


class CreateContractingAttributes(Wizard):
    'Create Contracting Attributes'
    __name__ = 'staff.payroll.create_contracting_attributes'
    start_state = 'create_attributes'
    create_attributes = StateTransition()

    def transition_create_attributes(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        employee_id = Transaction().context['active_id']
        employee = Employee(employee_id)
        employee.set_default_values()
        return 'end'


class CreateMedicalExamStart(ModelView):
    'Create Medical Exam Start'
    __name__ = 'staff.employee.create_medical_exam.start'
    laboratory = fields.Many2One('party.party', 'Laboratory', required=True)
    cause = fields.Selection([
            ('entry', 'Entry'),
            ('retirement', 'Retirement'),
            ('periodic', 'Periodic'),
            ('post_incapable', 'Post Incapable'),
            ('other', 'Other'),
        ], 'Cause', required=True)
    exam_date = fields.Date('Date', required=True, select=True)
    position = fields.Many2One('staff.position', 'Position', required=True)

    @staticmethod
    def default_exam_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateMedicalExam(Wizard):
    'Create Medical Exam'
    __name__ = 'staff.employee.create_medical_exam'
    start = StateView('staff.employee.create_medical_exam.start',
        'staff_contracting.create_medical_exam_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Employee = pool.get('company.employee')
        MedicalExam = pool.get('staff.employee.medical_exam')
        MedicalExamLine = pool.get('staff.medical_exam.line')

        employees_ids = Transaction().context['active_ids']
        employees = Employee.browse(employees_ids)
        values = []
        for employee in employees:
            val = {
                'employee': employee.id,
                'exam_date': self.start.exam_date,
                'laboratory': self.start.laboratory.id,
                'cause': self.start.cause,
                'state': 'draft',
            }
            medical_exam, = MedicalExam.create([val])
            medical_exam.save()

            for exam in self.start.position.medical_exams:
                val = {
                    'employee_medical_exam': medical_exam.id,
                    'exam': exam.id,
                    'notes': exam.description,
                }
                values.append(val)
            MedicalExamLine.create(values)
        return 'end'
