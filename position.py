#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta


class Position(metaclass=PoolMeta):
    __name__ = 'staff.position'
    medical_exams = fields.Many2Many(
            'staff_position.medical_exam',
            'position', 'medical_exam',
            'Position - Medical Exam')


class PositionMedicalExamDefault(ModelSQL):
    'Position - Medical Exam'
    __name__ = 'staff_position.medical_exam'
    _table = 'staff_position_medical_exam'
    position = fields.Many2One('staff.position',
            'Position', select=True, required=True,
            ondelete='CASCADE')
    medical_exam = fields.Many2One('staff.medical_exam',
            'Medical Exam', ondelete='CASCADE', select=True,
            required=True)
