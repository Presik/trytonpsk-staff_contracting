# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class StaffConfiguration(metaclass=PoolMeta):
    __name__ = 'staff.configuration'
    shirt = fields.Many2One('staff.uniform.element', 'Element Shirt')
    pants = fields.Many2One('staff.uniform.element', 'Element Pants')
    boots = fields.Many2One('staff.uniform.element', 'Element Boots')
