# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval

STATES = {'readonly': Eval('state') != 'draft'}


class OfficeSpecific(ModelSQL, ModelView):
    "Staff OfficeSpecific"
    __name__ = 'staff.office_specific'
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(OfficeSpecific, cls).__setup__()
        cls._order.insert(0, ('id', 'DESC'))
