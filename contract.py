# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool
from decimal import Decimal
import datetime
from trytond.i18n import gettext
from .exceptions import ContractValidationError


clasification_act = [
    ('operative', 'Operative'),
    ('not operative', 'Not operative'),
    ('', ''),
]

empl_benefits = [
    ('legal', 'Legal'),
    ('conventional', 'Conventional'),
    ('', ''),
]

subcontracting_ = [
    ('yes', 'Yes'),
    ('no', 'No'),
    ('', ''),
]

builder_empl_ = [
    ('yes', 'Yes'),
    ('no', 'No'),
    ('', ''),
]


class Contract(metaclass=PoolMeta):
    __name__ = 'staff.contract'

    workInstalationClient = fields.Boolean('Works Location Client',
        help='Check this option if the employee works \n' 
        'inside the premises of the contracting client')
    subdivision_activity = fields.Many2One('party.department_code',
        'Department', states={'required': ~Eval('workInstalationClient', False)})
    city_activity = fields.Many2One('party.city_code', 'City',
        domain=[('department', '=', Eval('subdivision_activity'))],
        states={'required': ~Eval('workInstalationClient', False)})
    address_activity = fields.Char('Address', states={'required': ~Eval('workInstalationClient', False)})
    local_workforce = fields.Boolean('Local Workforce')
    clasification_activity = fields.Selection(
        clasification_act, 'Clasification activity')
    employment_benefits = fields.Selection(
        empl_benefits, 'Type Employment Benefits', depends=['clasification_activity'])
    required_formation = fields.Boolean('Required Formation Professional',
        help='Check this option if the position \n' 
        'required formation Professional or technique')
    code_vacant = fields.Char('Code Vacant', select=True, 
        help='Fill this option if exist a code of vacant \n'
        ' in public service of employment')
    subcontracting = fields.Selection(subcontracting_, 'Subcontracting')
    nit_subcontracting = fields.Integer('Nit Subcontracting',
        states={
            'invisible': ~Eval('subcontracting').in_(['yes']),
            },
        depends=['subcontracting'])
    first_job = fields.Boolean('First Job',
        help='Check this option if this is the first job for a employee')
    integral_salary = fields.Boolean('Integral Salary',
        help='Check this option if employee have integral salary')
    high_pension_risk = fields.Boolean('High Pension Risk',
        help='Check this option if employee have high pension risk')
    builder_employee = fields.Selection(
        builder_empl_, 'Builder Employee')
    subcontracting_string = subcontracting.translated('subcontracting')
    events_vacations = fields.Many2Many(
        'staff.contract.event', 'contract', 'event', 'History Vacations')
    time_worked = fields.Function(fields.Numeric('Worked Days', digits=(16, 0)),
        'get_time_worked')
    days_enjoy = fields.Function(fields.Numeric(
        'Days Enjoy', digits=(16, 0)), 'get_days')
    days_for_enjoy = fields.Function(fields.Numeric(
        'Days For Enjoy', digits=(16, 0)), 'get_days')
    variable_salary = fields.Boolean('Variable Salary')

    @classmethod
    def __setup__(cls):
        super(Contract, cls).__setup__()
        new_sel = [
                ('steady_conventional', 'Steady Conventional'),
        ]
        if new_sel not in cls.kind.selection:
            cls.kind.selection.extend(new_sel)

    def default_builder_employee():
        return 'no'

    def default_workInstalationClient():
        return False

    def get_time_worked(self, name=None):
        start_date = self.start_date
        end_date = datetime.date.today()
        if self.end_date and self.finished_date and self.finished_date < end_date:
            end_date = self.finished_date
        return self.get_time_days(start_date, end_date)

    def get_time_days(self, start_date=None, end_date=None):
        if self.employment_benefits and self.employment_benefits == "conventional":
            return (end_date - start_date).days + 1
        res = super(Contract, self).get_time_days(start_date, end_date)
        return res

    def get_days(self, name):
        res = 0
        if name == 'days_enjoy':
            events = self.events_vacations
            for e in events:
                res += e.days_of_vacations if e.days_of_vacations else 0
        elif name == 'days_for_enjoy':
            res = Decimal(round((self.time_worked/30*1.25), 0)) - \
                          self.days_enjoy
        return res

    @classmethod
    def validate(cls, contracts):
        for contract in contracts:
            contract.check_employment_benefits()

    def check_employment_benefits(self):
        if self.clasification_activity and self.employment_benefits:
            if self.clasification_activity == 'not operative' and self.employment_benefits != 'legal':
                raise ContractValidationError(
                    gettext('staff_contracting.msg_error_employment_benefits'))


class ContractEvent(ModelSQL):
    "Contract - Events"
    __name__ = "staff.contract.event"
    _table = 'staff_contract_event_rel'

    contract = fields.Many2One('staff.contract', 'Contract',
                               ondelete='CASCADE', select=True, required=True)
    event = fields.Many2One('staff.event', 'Event',
                            ondelete='CASCADE', select=True, required=True)
